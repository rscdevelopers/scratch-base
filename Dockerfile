FROM gcc:latest AS compile

COPY src /src
WORKDIR /src
RUN gcc -static sleep.c -o sleep
RUN chmod +x ./sleep
RUN gcc -static noop.c -o noop
RUN chmod +x ./noop

FROM scratch

COPY --from=compile /src/sleep /sleep
COPY --from=compile /src/noop /noop

CMD ["/sleep"]
