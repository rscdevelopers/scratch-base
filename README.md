# Scratch base

A small-as-possible but runnable base image based on `FROM scratch`.

Every docker container needs a CMD, and if you really do not want one, you could use `CMD ["sleep", "infinity"]`.
However, this does not work with `FROM scratch`, since there are no binaries in the scratch image.
To solve that problem, this image includes the following two little binaries:

## `sleep`

It sleeps indefinitely.
This is the default CMD for this image.

## `noop`

Does nothing and exits with code 0. Essentially a no-op. 
This can be used as a health check executable, or as the `CMD` for the container, if you want it to exit immediately.
